require 'mechanize'
require 'terminal-notifier'
require 'byebug'
require 'json'
require 'logger'

module Alert
  def send_alert(prev_apts = [])
    newest_apts = self.get_newest
    if newest_apts == prev_apts
      apts = prev_apts
    else
      new_apts = newest_apts - prev_apts
      new_apts.each do |apt|
        prev_log = File.open('log') { |f| f.grep(Regexp.new(apt[0])) }.count
        # don't send alert when apt title already logged
        unless prev_log > 0
          File.open('log', 'a') do |f|
            f << "#{Time.now.strftime("%d-%m-%Y %H:%M")} \n"
            f << "#{apt[0]} \n"
            f << "#{apt[1]} \n"
            f << "\n"
          end
          puts '=' * 40
          puts "#{apt[0]} => #{apt[1]}"
          %x(terminal-notifier -message "#{apt[0]}" -title '🏡🏡🏡🏡🏡🏡🏡🏡🏡🏡🏡🏡' -open "#{apt[1]}")
        end
      end
      apts = newest_apts
    end
    sleep 2
    self.send_alert(apts)
  end
end

class CasaSapo
  include Alert

  def get_newest
    @agent = Mechanize.new
    page = @agent.get('https://casa.sapo.pt/Alugar/Apartamentos/T1-ate-T6-ou-superior/Lisboa/?sa=11&gp=1000&or=10')
    page.search('.searchResultProperty')[0..2].map do |apt|
      ["CasaSapo: #{apt.at('a')['title']}", "https://casa.sapo.pt#{apt.at('a')['href']}"]
    end
  end
end

class Century21
  include Alert

  def get_newest
    @agent = Mechanize.new
    url = 'http://www.century21.pt/en/properties-to-rent/?b=1&v=c&ord=date-desc&page=1&numberOfElements=12&ba=&be=1&map=1000&mip=&q=Portugal,+Lisboa,+Lisboa&agencyId=&pt=1&pstl'
    doc = @agent.get('http://www.century21.pt/umbraco/Surface/C21PropertiesSearchListingSurface/GetAllSEO?b=1&v=c&ord=date-desc&page=1&numberOfElements=12&ba=&be=1&map=1000&mip=&q=Portugal%2C%2BLisboa%2C%2BLisboa&agencyId=&pt=1&pstl=&mySite=False&masterId=1&seoId=&nodeId=32965&language=en&triggerbyAddressLocationLevelddl=false&AgencySiteLogoDefault=%2Fmedia%2F2862%2Flogo-pt.png&AgencySite_showAllAgenciesProperties=false&AgencyExternalName=')
    JSON.parse(doc.body)['Properties'][0..2].map do |apt|
      ["Century21: #{apt['Id']} - #{apt['Location']}", url]
    end
  end
end

class Imovirtual
  include Alert

  def get_newest
    @agent = Mechanize.new
    page = @agent.get('https://www.imovirtual.com/arrendar/apartamento/lisboa/?search%5Bfilter_float_price%3Ato%5D=1000&search%5Bdescription%5D=1&search%5Border%5D=created_at_first%3Adesc&search%5Bsubregion_id%5D=153&min_id=8843758')
    page.at('.col-md-content').search('article')[0..2].map do |apt|
      ["Imovirtual: #{apt.at('.offer-item-title').text}", apt.at('.offer-item-header').at('a')['href']]
    end
  end
end

class Olx
  include Alert

  def get_newest
    @agent = Mechanize.new
    page = @agent.get('https://www.olx.pt/imoveis/apartamento-casa-a-venda/encarnazao/?search%5Bfilter_enum_condicao%5D%5B0%5D=usado&search%5Bfilter_enum_condicao%5D%5B1%5D=renovado&search%5Bfilter_enum_condicao%5D%5B2%5D=novo&search%5Bfilter_float_price%3Ato%5D=1000&search%5Bfilter_enum_tipologia%5D%5B0%5D=t1&search%5Bfilter_enum_tipologia%5D%5B1%5D=t2&search%5Bfilter_enum_tipologia%5D%5B2%5D=t3&search%5Bfilter_enum_tipologia%5D%5B3%5D=t4&search%5Bdescription%5D=1&search%5Bdist%5D=5')
    page.at('#offers_table').search('.wrap')[0..2].map do |apt|
      ["OLX: #{apt.at('strong').text}", apt.at('.link')['href']]
    end
  end
end

class LiveInLisbon
  include Alert

  def get_newest
    @agent = Mechanize.new
    page = @agent.get('https://www.lil.pt/?fwp_negocio=arrendar&fwp_preco=0.00%2C1000.00')
    page.search('.quarto')[0..2].map do |apt|
      ['new apt on LiveInLisbon', apt.at('a')['href']]
    end
  end
end

class Matriz
  include Alert

  def get_newest
    @agent = Mechanize.new
    page = @agent.get('http://matrizalfacinha.pt/imoveis/?orc=6&prr=0%2C1100&bus=2&tyr=1%2C6&gnt=1&twn=153&pss=BasicPropertySearchFormLimit11U0S')
    page.search('.tarpRow')[0..2].map do |apt|
      ["Matriz: #{apt.at('.title').text.strip}", "http://matrizalfacinha.pt#{apt.at('a')['href']}"]
    end
  end
end

cs = Thread.new{ CasaSapo.new.send_alert }
c21 = Thread.new{ Century21.new.send_alert }
im = Thread.new{ Imovirtual.new.send_alert }
olx = Thread.new{ Olx.new.send_alert }
lil = Thread.new{ LiveInLisbon.new.send_alert }
ma = Thread.new{ Matriz.new.send_alert }
cs.join
c21.join
im.join
olx.join
lil.join
ma.join
